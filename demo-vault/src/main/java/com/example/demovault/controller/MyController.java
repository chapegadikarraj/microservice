package com.example.demovault.controller;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.vault.support.SslConfiguration;

import com.example.demovault.config.MyConfiguration;

@RestController
@RequestMapping("/demo-vault")
@EnableConfigurationProperties(MyConfiguration.class)
public class MyController {
	
//	@Autowired
//	private Environment environment;
	
	
	
	private MyConfiguration configuration;
	
	public MyController(MyConfiguration configuration) {
	super();
	this.configuration = configuration;
}

	@GetMapping(value = "/secret")
	public ResponseEntity<String> getSecret() {
		
		return ResponseEntity.ok(configuration.getPassword());
	}

}

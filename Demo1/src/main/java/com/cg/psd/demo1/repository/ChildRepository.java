package com.cg.psd.demo1.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.cg.psd.demo1.model.Child;
import com.cg.psd.demo1.model.Mother;

public interface ChildRepository extends MongoRepository<Child, String> {

	public List<Child> findByName(String name);
	public Optional<Child> findByChildId(String id);
	
	
	@Query("{ 'name': ?0 }")
	public List<Child> findByChildName(String name);
	
	@Query("{ 'name': { $regex : ?0 }}")
	public List<Child> findByChildNameStartingWith(String name);
	
	
}

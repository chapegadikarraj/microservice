package com.cg.psd.demo1.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.cg.psd.demo1.model.Mother;

public interface MotherRepository extends MongoRepository<Mother, String>{

	public List<Mother> findByName(String name);
	public Optional<Mother> findByMotherId(String id);
}

package com.cg.psd.demo1.sample;

public class Inner extends Outer implements Cloneable{

	private String inner;
	
	public String getInner() {
		return inner;
	}

	public void setInner(String inner) {
		this.inner = inner;
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
}

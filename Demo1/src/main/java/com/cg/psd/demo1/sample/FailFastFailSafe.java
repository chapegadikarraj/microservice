package com.cg.psd.demo1.sample;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class FailFastFailSafe {

	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		list.add("Raj");
		list.add("Hardik");
		
		Map<String, String> map = new ConcurrentHashMap<>();
		map.put("ha", "hardik");
		
		Collection<String> newList = Collections.synchronizedCollection(list);
		
		Iterator<String> i = newList.iterator();
		
		while(i.hasNext()){
			i.next();
//			i.remove();								cannot remove in fail-safe iterators
			
		}
		
		i = list.iterator();
		while(i.hasNext()){
			i.next();
//			i.remove();
			
		}
		
		Iterator<Map.Entry<String, String>> iter = map.entrySet().iterator();
		while(i.hasNext()){
			i.next();
			i.remove();
			
		}
		
	}
}

package com.cg.psd.demo1.controllers;

import java.net.URI;
import java.util.List;
import java.util.UUID;

//import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.cg.psd.demo1.model.Child;
import com.cg.psd.demo1.model.Mother;
import com.cg.psd.demo1.model.Profile;
import com.cg.psd.demo1.proxy.Demo2Proxy;
import com.cg.psd.demo1.repository.ChildRepository;
import com.cg.psd.demo1.repository.MotherRepository;
import com.cg.psd.demo1.service.ProfileService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
@RequestMapping(value="/demo1")
public class SampleController {
	
	static int count=0;
	
	@Value("${Demo1.name:name}")
	private String name;
	
	@Autowired
	private Demo2Proxy proxy;
	
	@Autowired
	private Environment environemnt;
	
	@Autowired
	private ProfileService service;
	
	@Autowired
	private MotherRepository motherRepo;
	
	@Autowired
	private ChildRepository childRepo;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@RequestMapping(value="/funPost",method=RequestMethod.POST, produces="application/xml")
	public Profile funPost(
			/* @Valid */@RequestBody Profile profile, 
			@RequestHeader(value= "HEADER1", required=false) String header1){
			service.saveProfile(profile);
		return profile;
		
	}

	@RequestMapping(value="/funGet/{name}", method=RequestMethod.GET)
	@HystrixCommand(fallbackMethod = "fallBack")
	public String funGet(
			@RequestHeader(value= "HEADER1", required=false) String header1,
			@PathVariable(value="name", required= true) String name){
//		throw new RuntimeException("hystrix");
			String str = new RestTemplate().postForObject(URI.create("http://localhost:8083/demo2/funPost"), new Profile(), String.class);
		return str;
		
	}
	
	@RequestMapping(value="/funGetFeign/{name}", method=RequestMethod.GET)
	public String funGetFeign(
			@RequestHeader(value= "Authorization", required=false) String header1,
			@PathVariable(value="name", required= true) String name){
			String str = proxy.funPost(new Profile(), header1);
		return "demo2: "+str+" demo1: "+environemnt.getProperty("local.server.port");
		
	}
	
	@RequestMapping(value="/funException", method=RequestMethod.GET)
	public String funException(
			@RequestHeader(value= "HEADER1", required=false) String header1){
		if(true)
			throw new RuntimeException("Inside demo1 controller");
		return "";
	}
	
	
	// fallback method should have same method signature.
	// If exception is thrown in calling method, then also fallback method is called. Flow will not
	// go to exception handler.
	private String fallBack(
			@RequestHeader(value= "HEADER1", required=false) String header1,
			@PathVariable(value="name", required= true) String name) {
		
		return "FallBack";
	}
	
	@RequestMapping(value="/mother", method=RequestMethod.GET)
	public String saveMother(
			@RequestHeader(value= "HEADER1", required=false) String header1){
		
		Mother m = new Mother();
		m.setMotherId(UUID.randomUUID().toString());
		m.setName("sandhya");
		motherRepo.save(m);
		return "done";
	}
	
	@RequestMapping(value="/getMother", method=RequestMethod.GET)
	public String getMother(
			@RequestHeader(value= "HEADER1", required=false) String header1){
		
		
		List<Mother> mother = motherRepo.findByName("sandhya");
		List<Child> children =  mother.get(0).getChildren();
		Child child = children.get(0);
		
		mongoTemplate.save(mother);
		mongoTemplate.find(new Query().addCriteria(Criteria.where("name").is("sandhya")), Mother.class);
		
		
		mongoTemplate.findAndModify(new Query().addCriteria(Criteria.where("name").is("sandhya")), 
					new Update().set("name", "Sandhya"), Mother.class);
		mongoTemplate.remove(new Query().addCriteria(Criteria.where("name").is("sandhya")), Mother.class);
		return "done";
		
	}
	
	
	
	@RequestMapping(value="/child", method=RequestMethod.GET)
	public String saveChild(
			@RequestHeader(value= "HEADER1", required=false) String header1){
		
		Child m = new Child();
		m.setChildId(UUID.randomUUID().toString());
		m.setName("pooja");
		childRepo.save(m);
		return "done";
	}
	
	
}

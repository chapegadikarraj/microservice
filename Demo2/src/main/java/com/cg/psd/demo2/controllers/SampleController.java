package com.cg.psd.demo2.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cg.psd.demo2.domain.Profile;


@RestController
@RequestMapping(value="/demo2")
public class SampleController {
	
	
	@Autowired
	private Environment environemnt;
	
	@RequestMapping(value="/funPost", method=RequestMethod.POST)
	public String funPost(
			@RequestBody Profile profile){
		
		return environemnt.getProperty("local.server.port");
		
	}

	@RequestMapping(value="/funGet/{name}", method=RequestMethod.GET)
	public String funGet(
			@PathVariable(value="name", required= true) String name){
		return name;
		
	}
}